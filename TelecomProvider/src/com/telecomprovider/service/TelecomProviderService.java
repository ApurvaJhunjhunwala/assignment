package com.telecomprovider.service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.telecomprovider.model.TelecomProvider;

@Path("/telecomprovider")
public class TelecomProviderService {
	private TelecomProvider newCustomer=null;
	private List<Long> phoneNumbers = new ArrayList();
	private List <TelecomProvider> telecoProviderModelList=null;

	/*The API helps to retrieve all the phonenumbers of the customer.
	from the phone directory.
	/rest/telecomprovider

	 */
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public List <Long> getAllPhoneNumbers() throws Exception{
		if(telecoProviderModelList!=null && !telecoProviderModelList.isEmpty()){
			for(TelecomProvider telpp : telecoProviderModelList){

				phoneNumbers.add(telpp.getCustomerPhoneNumber());
			}
			return phoneNumbers;
		}
		else{
			throw new Exception("No Records found");
		}
	}

	/*The API helps to retrieve the phonenumbers of the specific customer.
			/rest/telecomprovider/id
	 */

	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Long getPhoneNumber(@PathParam("id") Integer customerId) throws Exception{
		Long number = null;
		if(telecoProviderModelList!=null && !telecoProviderModelList.isEmpty()){
			for(TelecomProvider telpp : telecoProviderModelList){
				if(telpp.getCustomerId()==customerId){
					number = telpp.getCustomerPhoneNumber();
				}else{
					throw new Exception("Customer Not found");
				}
			}

		}
		return number;
	}

	/*The API helps to Activate the customerNumber.
			/rest/telecomprovider/id/activateNumber

	 */
	@PUT
	@Path("{id}/{activateNumber}")
	@Produces(MediaType.APPLICATION_JSON)
	public String activateNumber(@PathParam("id") Integer customerId,@PathParam("activateNumber") String activateNumber) throws Exception{
		Long number = null;
		if(telecoProviderModelList!=null && !telecoProviderModelList.isEmpty()){
			for(TelecomProvider telpp : telecoProviderModelList){
				if(telpp.getCustomerId()==customerId){
					telpp.setActivateNumber(activateNumber);
					number = telpp.getCustomerPhoneNumber();
					
				}
				else{
					throw new Exception("Customer Not found");
				}
			}
		}
		return  number.toString()+"Number Activated";	
	}


	/*The API helps to Add new customerNumber.

			/rest/telecomprovider/add
	 */

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/add")
	public String addCustomer() throws Exception{
		TelecomProvider newCustomer = new TelecomProvider();
		int count =3;
		int id=1;
		for(int i=0;i<count;i++){
			newCustomer= new TelecomProvider();
			newCustomer.setCustomerId(id);
			newCustomer.setCustomerName("Franky"+id);
			newCustomer.setCustomerPhoneNumber(8989876545l);
			id++;
			telecoProviderModelList.add(newCustomer);

		}
		return "Customer Added";
	}
}
