package com.telecomprovider.model;

public class TelecomProvider {

	private int customerId;
	private String customerName;
	private Long customerPhoneNumber;
	private String activateNumber;
	public int getCustomerId() {
		return customerId;
	}
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public Long getCustomerPhoneNumber() {
		return customerPhoneNumber;
	}
	public void setCustomerPhoneNumber(Long customerPhoneNumber) {
		this.customerPhoneNumber = customerPhoneNumber;
	}
	public String getActivateNumber() {
		return activateNumber;
	}
	public void setActivateNumber(String activateNumber) {
		this.activateNumber = activateNumber;
	}
}
