package com.telecomprovider.application;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/service")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        
        Set<Class<?>> resources = new java.util.HashSet<>();
        
        System.out.println("REST configuration starting: getClasses()");            
        
        
        
        resources.add(com.telecomprovider.model.TelecomProvider.class);
        
        System.out.println("REST configuration ended successfully.");
        
        return resources;
    }
    
    @Override
    public Set<Object> getSingletons() {
        return Collections.emptySet();
    }
    
    public Map<String, Object> getProperties() {
        Map<String, Object> properties = new HashMap<>();
        
        properties.put("jersey.config.server.wadl.disableWadl", true);
        
        
                return properties;
    }    
}